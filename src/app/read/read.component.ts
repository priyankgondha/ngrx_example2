import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs'
import { Store } from '@ngrx/store';
import { AppState } from './../app.state';
import * as FormActions from './../actions/form.actions'
import { Form } from './../form/form.model'


@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {


  forms:Observable<Form[]>;

  constructor(private store:Store<AppState>) {
    this.forms = store.select('form')
  }

  deleteForms(index:number){
    this.store.dispatch(new FormActions.RemoveForm(index))
  }




  ngOnInit(): void {
  }

}
