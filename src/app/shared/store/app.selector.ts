/*
 * File Created: Wednesday, 21st September 2022 10:24:11 am                    *
 * Author: Priyank Gondha (priyank.gondha@solutionanalysts.in)                 *
 * -----                                                                       *
 * Last Modified: Wednesday, 28th September 2022 11:46:58 am                   *
 * Modified By: Priyank Gondha (priyank.gondha@solutionanalysts.in)            *
 * -----                                                                       *
 * Copyright - 2022 Solution Analysts                                          *
 */





import { createFeatureSelector } from '@ngrx/store';
import { Appstate } from './appstate';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


export const selectAppState = createFeatureSelector<Appstate>('appState');
