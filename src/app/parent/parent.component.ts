import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  @Input ('sendObj') sendObj;
  @Output ('reciveObj')  reciveObj  = new EventEmitter();

  show:boolean=true
  constructor() { }

  ngOnInit(): void {
    if(this.sendObj!=null){

      console.log(this.sendObj);
    }

  }

  childToParent(){
    this.sendObj={
      email:"priyankgondha@555gmail.com"
    }
    this.reciveObj.emit(this.sendObj)
    this.show=false;
  }

}
