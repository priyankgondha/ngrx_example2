/*
 * File Created: Monday, 10th October 2022 3:39:41 pm                          *
 * Author: Priyank Gondha (priyank.gondha@solutionanalysts.in)                 *
 * -----                                                                       *
 * Last Modified: Monday, 10th October 2022 3:47:37 pm                         *
 * Modified By: Priyank Gondha (priyank.gondha@solutionanalysts.in)            *
 * -----                                                                       *
 * Copyright - 2022 Solution Analysts                                          *
 */



import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";
@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor(private http: HttpClient) { }

  private baseUrl = environment.userUrl;

  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.baseUrl}upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  getFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/files`);
  }
}

