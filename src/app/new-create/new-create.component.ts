import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as FormActions from './../actions/form.actions'

@Component({
  selector: 'app-new-create',
  templateUrl: './new-create.component.html',
  styleUrls: ['./new-create.component.css']
})


export class NewCreateComponent implements OnInit {

  constructor(public store:Store<AppState>) { }

  ngOnInit(): void {
  }

  addForm(name:any,companyName:any,mobileNumber:any,email:any){

    console.log(name,companyName,mobileNumber,email);

    this.store.dispatch(new FormActions.AddForm({name:name,companyName:companyName,mobileNumber:mobileNumber,email:email}))
  }
}
