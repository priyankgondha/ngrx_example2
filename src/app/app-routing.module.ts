/*
 * File Created: Thursday, 15th September 2022 10:45:27 am                     *
 * Author: Priyank Gondha (priyank.gondha@solutionanalysts.in)                 *
 * -----                                                                       *
 * Last Modified: Monday, 10th October 2022 3:34:11 pm                         *
 * Modified By: Priyank Gondha (priyank.gondha@solutionanalysts.in)            *
 * -----                                                                       *
 * Copyright - 2022 Solution Analysts                                          *
 */



import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BootstapComponent } from './bootstap/bootstap.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { HttpCachingComponent } from './http-caching/http-caching.component';
import { InputOutputComponent } from './input-output/input-output.component';
import { MaterialDialogComponent } from './material-dialog/material-dialog.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { MaterialComponent } from './material/material.component';
import { NewCreateComponent } from './new-create/new-create.component';
import { NgrxComponent } from './ngrx/ngrx.component';

const routes: Routes = [
  { path: 'dialog', component: MaterialDialogComponent },
  { path:'table', component:MaterialTableComponent},
  { path:'ngrx', component:NgrxComponent},
  { path:'bootstrap', component:BootstapComponent},
  { path:'input-output', component:InputOutputComponent},
  { path:'http-caching',component:HttpCachingComponent},
  { path: 'bookstore',loadChildren:()=>import('./book/books.module').then((b)=>b.BooksModule)},
  { path:'material',component:MaterialComponent},
  { path:'file-upload',component:FileUploadComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
