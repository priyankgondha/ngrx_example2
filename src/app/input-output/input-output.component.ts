import { Component, OnInit,Input,Output, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.css']
})
export class InputOutputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @ViewChild('name') name!: ElementRef;
  @ViewChild('number') number!:ElementRef;
  display:boolean=false;
  reciveOBJ:any

  sendObj:any;

  sendData(){
    this.sendObj = {
      name:this.name.nativeElement.value,
      number:this.number.nativeElement.value
    }

    console.log(this.sendObj);

  }


  reciveObjFromParent(reciveObj:any){
    this.display=true
    this.reciveOBJ = reciveObj;
    console.log(reciveObj);
  }


}
