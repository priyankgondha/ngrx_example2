import { Component, OnInit,TemplateRef, ViewChild } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { MyDialogComponent } from '../my-dialog/my-dialog.component';

@Component({
  selector: 'app-material-dialog',
  templateUrl: './material-dialog.component.html',
  styleUrls: ['./material-dialog.component.css']
})
export class MaterialDialogComponent implements OnInit {

  @ViewChild('dialogRef') dialogRef!: TemplateRef<any>;
  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {

  }

  myFooList = ['Some Item', 'Item Second', 'Other In Row', 'What to write', 'Blah To Do']
  myNameList = ['Priyank','Karan','Utam','Krushik']

  openTempDialog() {
    const myTempDialog = this.dialog.open(this.dialogRef, { data: this.myFooList });
    myTempDialog.afterClosed().subscribe((res) => {

      // Data back from dialog
      console.log({ res });
    });

    myTempDialog.beforeClosed().subscribe((res)=>{
      console.log({ res });
    })

    myTempDialog.afterOpened().subscribe((res)=>{
      console.log({ res });
    })


  }

responce:any;
getRes2:boolean=false
  openCompDialog() {
    this.getRes2 = false;
    const myCompDialog = this.dialog.open(MyDialogComponent, { data: this.myNameList });
    myCompDialog.afterClosed().subscribe((res) => {
      // Data back from dialog
      this.responce = res;
      if(this.responce!=null){
        this.getRes2 = true;
      }
    });
  }

}
