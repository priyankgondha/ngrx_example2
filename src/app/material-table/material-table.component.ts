import { Component, OnInit } from '@angular/core';
import { AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { PeriodicElement } from './demo.interface';
import {  CdkDragDrop, moveItemInArray,transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-material-table',
  templateUrl: './material-table.component.html',
  styleUrls: ['./material-table.component.css'],
})
export class MaterialTableComponent implements OnInit {

  [x: string]: any;
  // displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  displayedColumns: string[] = [];
  dataSource:any;
  dataSource_dual:any;
  columnsToDisplay: string[] = [];
  dragDisabled = true;
  @ViewChild(MatTable)table!: MatTable<any>;
  // @ViewChild('table', { static: true }) table!: MatTable<PeriodicElement>;
  // @ViewChild('tabledual', { static: true }) tableDual!: MatTable<PeriodicElement>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  origin: any = [
    {
      name: 'position',
      isSet: true,
      index: 0,
    },
    {
      name: 'name',
      isSet: true,
      index: 0,
    },
    {
      name: 'weight',
      isSet: true,
      index: 0,
    },
    {
      name: 'symbol',
      isSet: true,
      index: 0,
    },
  ];

  constructor() {
    this.origin.forEach((element) => {
      this.displayedColumns.push(element.name);
    });
    this.columnsToDisplay = this.displayedColumns.slice();
    console.log(this.displayedColumns);
    console.log(this.columnsToDisplay);
    
  }

  async ngOnInit() {
    this.dataSource = await ELEMENT_DATA;
    this.dataSource_dual = await ELEMENT_DATA_DUAL;

    // this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    // this.dataSource_dual = new MatTableDataSource(ELEMENT_DATA_DUAL);
  }

  // ngAfterViewInit() {
  //   // this.dataSource.paginator = this.paginator;
  //   this.columnsToDisplay = this.displayedColumns.slice();
  //   this.dragDisabled = true;
  // }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event);
    // this.dragDisabled = true;
    // const previousIndex = this.dataSource.findIndex(
    //   (d: any) => d === event.item.data
    // );
    // console.log(previousIndex);

    // moveItemInArray(this.dataSource, previousIndex, event.currentIndex);
    // this.table.renderRows();
    
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
    }
    this.dataSource = new MatTableDataSource(this.dataSource);
    this.dataSource_dual = new MatTableDataSource(this.dataSource_dual);
    // this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    // this.dataSource_dual = new MatTableDataSource(ELEMENT_DATA_DUAL);



  }

  addCol() {
    const randomColumn = Math.floor(
      Math.random() * this.displayedColumns.length
    );
    console.log(randomColumn);
    // this.columnsToDisplay.push(this.displayedColumns[randomColumn]);
    // console.log(this.displayedColumns);
    // console.log(this.columnsToDisplay);
    this.columnsToDisplay.push('symbol');
  }
}

// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  // { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  // { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  // { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  // { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  // { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  // { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  // { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  // { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
  // { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  // { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  // { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  // { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  // { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  // { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  // { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
  // { position: 21, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  // { position: 22, name: 'Helium', weight: 4.0026, symbol: 'He' },
  // { position: 23, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  // { position: 24, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  // { position: 25, name: 'Boron', weight: 10.811, symbol: 'B' },
  // { position: 26, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  // { position: 27, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  // { position: 28, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  // { position: 29, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  // { position: 30, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  // { position: 31, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  // { position: 32, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  // { position: 33, name: 'Aluminum', weight: 26.9835, symbol: 'Al' },
  // { position: 34, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  // { position: 35, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  // { position: 36, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  // { position: 37, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  // { position: 38, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  // { position: 39, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  // { position: 40, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];

const ELEMENT_DATA_DUAL: PeriodicElement[] = [
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' }
]
