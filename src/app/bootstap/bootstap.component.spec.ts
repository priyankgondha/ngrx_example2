import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BootstapComponent } from './bootstap.component';

describe('BootstapComponent', () => {
  let component: BootstapComponent;
  let fixture: ComponentFixture<BootstapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BootstapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BootstapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
