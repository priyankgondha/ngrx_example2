import { Form } from '../form/form.model'
import * as FormActions from './../actions/form.actions'

const intialState: Form = {
  name:'Priyank Patel',
  companyName:'Solution Analysts',
  mobileNumber:9574931319,
  email:'priyankgondha555@gmail.com'
}


export function reducer(state:Form[]=[intialState],action:FormActions.Actions){

  switch(action.type){

    case FormActions.ADD_FORM:
    return[...state,action.payload]

    case FormActions.REMOVE_FORM:
      const index = action.payload;
      return [...state.slice(0,index),...state.slice(index+1)]

    default:
      return state;

  }
}
