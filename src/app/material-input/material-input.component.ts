import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-material-input',
  templateUrl: './material-input.component.html',
  styleUrls: ['./material-input.component.css']
})

export class MaterialInputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


    nameFormControl = new FormControl('', [Validators.required]);
    dateFormControl = new FormControl('', [Validators.required]);
    lastNameFormControl = new FormControl('', [Validators.required]);
    designationFormControl = new FormControl('', [Validators.required]);
    mobileFormControl = new FormControl('', [Validators.required]);
    genderFormControl = new FormControl('', [Validators.required]);
    emailFormControl = new FormControl('', [Validators.required, Validators.email]);


    genders= [
      {value: 'male', viewValue: 'Male'},
      {value: 'female', viewValue: 'Female'},
    ];

    submit(){

      let obj={
        Name:this.nameFormControl.value,
        LastName:this.lastNameFormControl.value,
        Email:this.emailFormControl.value,
        Designation:this.designationFormControl.value,
        MobileNumber:this.mobileFormControl.value,
        Dob:this.dateFormControl.value,
        Gender:this.genderFormControl.value

      }
      console.log(obj);

    }




  }
