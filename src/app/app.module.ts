import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ActionReducerMap,StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReadComponent } from './read/read.component';
import { NewCreateComponent } from './new-create/new-create.component';
import { reducer } from './../app/reducers/form.reducers';
import { BootstapComponent } from './bootstap/bootstap.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MaterialComponent } from './material/material.component'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule} from '@angular/material/datepicker'
import { MatFormFieldModule} from '@angular/material/form-field'
import { MatNativeDateModule } from '@angular/material/core';
import {MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialDialogComponent } from './material-dialog/material-dialog.component';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { MatSelectModule } from '@angular/material/select';
import {AfterViewInit,ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MaterialInputComponent } from './material-input/material-input.component';
import { NgrxComponent } from './ngrx/ngrx.component';
import { InputOutputComponent } from './input-output/input-output.component';
import { ParentComponent } from './parent/parent.component';
import { HttpCachingComponent } from './http-caching/http-caching.component';
import { HttpClientModule } from '@angular/common/http';
import { BookComponent } from './book/book.component';
import { EffectsModule } from '@ngrx/effects';
import { appReducer } from './shared/store/app.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import { DragDropModule} from '@angular/cdk/drag-drop';


@NgModule({
  declarations: [
    AppComponent,
    ReadComponent,
    NewCreateComponent,
    BootstapComponent,
    MaterialComponent,
    MaterialDialogComponent,
    MyDialogComponent,
    MaterialTableComponent,
    MaterialInputComponent,
    NgrxComponent,
    InputOutputComponent,
    ParentComponent,
    HttpCachingComponent,
    BookComponent,
    FileUploadComponent,

  ],
  imports: [
    BrowserModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    HttpClientModule,
    MatDialogModule,
    AppRoutingModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatInputModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatTableModule,
    NgMultiSelectDropDownModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot({ appState: appReducer,form :reducer }as ActionReducerMap<any,any>),
    // StoreModule.forRoot({form :reducer} as ActionReducerMap<any,any>),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    FontAwesomeModule
  ],
  providers: [],
  entryComponents:[MatDialogModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
