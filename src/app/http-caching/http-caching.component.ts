import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, startWith } from 'rxjs';

const CACHE_KEY = 'httpRepoCache';

@Component({
  selector: 'app-http-caching',
  templateUrl: './http-caching.component.html',
  styleUrls: ['./http-caching.component.css']
})


export class HttpCachingComponent implements OnInit {

  countries:any = [
    {
      item_id: 1,
      item_text: "India",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/India.jpg"
    },
    {
      item_id: 5,
      item_text: "Israel",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Israel.jpg"
    }
  ];
  selCountries = [
    {
      item_id: 1,
      item_text: "India",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/India.jpg"
    },
    {
      item_id: 5,
      item_text: "Israel",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Israel.jpg"
    }
  ];
  dropdownSettings: any = {
    idField: 'item_id',
    defaultOpen:false,
    singleSelection: false,
    textField: "item_text",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };

  ngOnInit() {
    
    
    // this.dropdownSettings = 
    console.log(this.countries);
    
  }
  repos: any;
  constructor(http: HttpClient) {
  //   // get data from api
  //   const path = 'https://api.github.com/search/repositories?q=angular';
  //   this.repos = http.get<any>(path).pipe(map((data) => data.items));

  //   // localstorage filter data
  //   this.repos.subscribe((next: any) => {
  //     localStorage[CACHE_KEY] = JSON.stringify(next);
  //   });

  //   this.repos = this.repos.pipe(startWith(JSON.parse(localStorage[CACHE_KEY] || '[]')));
  this.countries = [
    {
      item_id: 1,
      item_text: "India",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/India.jpg"
    },
    {
      item_id: 2,
      item_text: "Spain",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Spain.jpg"
    },
    {
      item_id: 3,
      item_text: "United Kingdom",
      image:
        "http://www.sciencekids.co.nz/images/pictures/flags96/United_Kingdom.jpg"
    },
    {
      item_id: 4,
      item_text: "Canada",
      image:
        "http://www.sciencekids.co.nz/images/pictures/flags96/Canada.jpg",
      isDisabled: true
    },
    {
      item_id: 5,
      item_text: "Israel",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Israel.jpg"
    },
    {
      item_id: 6,
      item_text: "Brazil",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Brazil.jpg"
    },
    {
      item_id: 7,
      item_text: "Barbados",
      image:
        "http://www.sciencekids.co.nz/images/pictures/flags96/Barbados.jpg"
    },
    {
      item_id: 8,
      item_text: "Mexico",
      image: "http://www.sciencekids.co.nz/images/pictures/flags96/Mexico.jpg"
    }
  ];

  }

  get getItems() {
    return this.countries.reduce((acc, curr) => {
      acc[curr.item_id] = curr;
      return acc;
    }, {});
  }

  onItemSelect(item: any) {
    console.log("onItemSelect", item);
  }
  onSelectAll(items: any) {
    console.log("onSelectAll", items);
  }
}
