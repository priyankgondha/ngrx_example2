import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { bookReducer} from './store/books.reducer'
import { BooksEffect } from './store/books.effect';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { HomeComponent } from './home/home.component';
import { BooksRoutingModule } from './books-routing.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AddComponent,
    EditComponent,
    HomeComponent,
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,
    FormsModule,
    StoreModule.forFeature('mybooks', bookReducer),
    EffectsModule.forFeature([BooksEffect])
  ]
})
export class BooksModule { }
