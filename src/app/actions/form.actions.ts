import {Action} from'@ngrx/store';
import { Form } from '../form/form.model'

export const ADD_FORM = '[FORM] Add'
export const REMOVE_FORM = '[FORM] Remove'


export class AddForm implements Action{
  readonly type = ADD_FORM

    constructor(public payload: Form) {}
}


export class RemoveForm implements Action{
  readonly type = REMOVE_FORM;
  constructor(public payload:number){}
}

export type Actions = AddForm | RemoveForm
