import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {FormControl, FormGroup, Validators,FormBuilder} from '@angular/forms';


@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.css']
})
export class MyDialogComponent implements OnInit {

  fromPage!: string;
  fromDialog!: string;

  myobj:{
    start:string,
    end:string,
    range:string
  }={
    start:'',
    end:'',
    range:''
  };
  dateStart!:string;
  dateEnd!:string;
  form!:FormGroup

  constructor(
    public dialogRef: MatDialogRef<MyDialogComponent>,private formBuilder: FormBuilder,
    @Optional() @Inject(MAT_DIALOG_DATA) public mydata: any

    ) { }

    ngOnInit(): void {
    console.log("Coming data from Parent Component:",this.mydata);
    // this.fromDialog = "I am from dialog COMPONENT...";
    this.form = this.formBuilder.group({
      nameFormControl : [null, [Validators.required]],
      dateFormControl : [null, [Validators.required]],
      lastNameFormControl: [null, [Validators.required]],
      designationFormControl:[null, [Validators.required]],
      mobileFormControl: [null, [Validators.required]],
      genderFormControl: [null, [Validators.required]],
      emailFormControl: [null, [Validators.required, Validators.email]]
    })

  }

  closeDialog() { this.dialogRef.close({ event: 'close', data: this.myobj}); }

  dateRangeChange(dateRangeStart, dateRangeEnd){
    this.dateStart = dateRangeStart.value
    this.dateEnd = dateRangeEnd.value
    this.myobj.start = this.dateStart
    this.myobj.end = this.dateEnd;
  }

  addData(){
      this.myobj.range="Range"
  }


  // nameFormControl = new FormControl('', [Validators.required]);
  // dateFormControl = new FormControl('', [Validators.required]);
  // lastNameFormControl = new FormControl('', [Validators.required]);
  // designationFormControl = new FormControl('', [Validators.required]);
  // mobileFormControl = new FormControl('', [Validators.required]);
  // genderFormControl = new FormControl('', [Validators.required]);
  // emailFormControl = new FormControl('', [Validators.required, Validators.email]);


  genders= [
    {value: 'male', viewValue: 'Male'},
    {value: 'female', viewValue: 'Female'},
  ];


  formObj:any;
  submit(form:any){
console.log(form.controls.nameFormControl.value);

    if(this.form.valid){

      this.formObj={
        Name:form.controls.nameFormControl.value,
        LastName:form.controls.lastNameFormControl.value,
        Email:form.controls.emailFormControl.value,
        Designation:form.controls.designationFormControl.value,
        MobileNumber:form.controls.mobileFormControl.value,
        Dob:form.controls.dateFormControl.value,
        Gender:form.controls.genderFormControl.value

      }
      console.log(this.formObj);
      this.dialogRef.close({ event: 'close', data:this.formObj});
    }
  }



}
