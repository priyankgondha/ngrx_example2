/*
 * File Created: Thursday, 15th September 2022 10:45:27 am                     *
 * Author: Priyank Gondha (priyank.gondha@solutionanalysts.in)                 *
 * -----                                                                       *
 * Last Modified: Monday, 10th October 2022 3:45:12 pm                         *
 * Modified By: Priyank Gondha (priyank.gondha@solutionanalysts.in)            *
 * -----                                                                       *
 * Copyright - 2022 Solution Analysts                                          *
 */



// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  userUrl:"http://192.168.3.112:5001/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
